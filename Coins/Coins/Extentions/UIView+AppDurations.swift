//
//  UIView+AppDurations.swift
//  Coins
//
//  Created by Алексей Петров on 26/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    static var defaultDuration: TimeInterval {
        return 0.25
    }
}
