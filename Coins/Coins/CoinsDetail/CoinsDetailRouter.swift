//
//  CoinsDetailRouter.swift
//  Coins
//
//  Created by Алексей Петров on 30/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation

class CoinsDetailRouter: CoinsDetailRouterProtocol {
    weak var viewController: CoinsDetailViewController!
    
    init(viewController: CoinsDetailViewController) {
        self.viewController = viewController
    }
}
