//
//  GlobalInfoRouter.swift
//  Coins
//
//  Created by Алексей Петров on 08/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class GlobalInfoRouter: GlobalInfoRouterProtocol {
    
    weak var viewController: GlobalInfoViewController!
    
    init(viewController: GlobalInfoViewController) {
        self.viewController = viewController
    }

}
