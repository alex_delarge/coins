//
//  CodingUserInfoKey+context.swift
//  Coins
//
//  Created by Алексей Петров on 29/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}
