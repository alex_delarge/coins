//
//  CoinData+CoreDataProperties.swift
//  
//
//  Created by Алексей Петров on 21/02/2019.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension CoinData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CoinData> {
        return NSFetchRequest<CoinData>(entityName: "CoinData")
    }

    @NSManaged public var circlatingSupply: Double
    @NSManaged public var cmcRank: Double
    @NSManaged public var dateAdded: String?
    @NSManaged public var id: Double
    @NSManaged public var lastUpdate: String?
    @NSManaged public var lastUpdated: String?
    @NSManaged public var marketCap: Double
    @NSManaged public var name: String?
    @NSManaged public var numMarketPairs: Double
    @NSManaged public var oneDayDynamic: Double
    @NSManaged public var oneHourDynamic: Double
    @NSManaged public var oneWeekDynamic: Double
    @NSManaged public var price: Double
    @NSManaged public var slug: String?
    @NSManaged public var symbol: String?
    @NSManaged public var tags: NSObject?
    @NSManaged public var totalMarketCap: Double
    @NSManaged public var totalSupply: Double
    @NSManaged public var volumeDay: Double

}
