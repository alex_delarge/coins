//
//  CoinsProtocols.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

protocol CoinsViewProtocol: class {
    var viewFrame: CGRect { get }
    func startReloadDataInTableView()
    func setCreateUITableViewCell(cell: UITableViewCell)
    func fetchCoinsToList(indexOfLastCoin: Int)
    func hideTableView(hide: Bool)
    func showInternetConnectionStatus(show: Bool)
    func startingButtonsState()
    func setupNavigationBar()
    func startShowingFavorites()
    func stopShowingFavorites()
    func changeBackView(center: CGPoint, widght: Double)
    func reloadRow(atIndexPath: IndexPath)
    func setCellSize(indexPath: IndexPath) -> CGFloat
    func hideInfo(atIndexPath: IndexPath)
    func showTableAutomaticly()
}

protocol CoinsPresenterProtocol: class {
    var router: CoinsRouterProtocol! { set get }
    func configureView()
    func configureCell(cell: CoinTablevVewCell, index: IndexPath) -> UITableViewCell
    func setNumbersOfRows() -> Int
    func fetchData(index:IndexPath)
    func didSelectRow(indexPath: IndexPath)
    func filterViewOpen()
    func reloadContext()
    func reloadContextAfterConnection ()
    func setCryprocyrrency(type: String)
    func confifCollectionCell(cell: UICollectionViewCell) -> UICollectionViewCell
    func getCellsCount(section : Int) -> Int
    func getSectionCount() -> Int
    func showFavorites()
    func changeBackView(center: CGPoint, widght: Double)
    func getCellSize(indexPath: IndexPath) -> CGFloat
    func didDeselectRow(indexPath: IndexPath)
}

protocol CoinsInteractorProtocol: class {
    func loadCoinsList(startIndex: Int, limit: Int, completition: @escaping (_ result: Array<CoinViewModel>?) -> Void)
    func setCryptocurrncy(type: String)
}

protocol CoinsRouterProtocol: class {
  func prepare(for segue: UIStoryboardSegue, sender: Any?)
  func showCoinDetail(coin: Coin)
     func showFilter()
}

protocol CoinsConfiguratorProtocol: class {
    func configure(with viewController: CoinsViewController)
    
    
}


protocol CoinsViewControllerDelegate: class {
    func didFinishTask()
    
}

