//
//  CoinsRoiter.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinsRouter: CoinsRouterProtocol {
    weak var viewController: CoinsViewController!
//    private var coin: Coin = C
    init(viewController: CoinsViewController) {
        self.viewController = viewController
    }
    
    func showCoinDetail(coin: Coin) {
//        self.coin = coin
        self.viewController.performSegue(withIdentifier: viewController.detailSegue, sender: nil)
       
    }
    
    func showFilter() {
        self.viewController.performSegue(withIdentifier: viewController.filterSegue, sender: nil)
        
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case viewController.detailSegue:
//             let destanationVC : CoinsDetailViewController = segue.destination as! CoinsDetailViewController
//            destanationVC.awakeFromStoryBoard(coin: self.coin)
            break
        case viewController.filterSegue :
//            let destanationNavVC: UINavigationController = segue.destination as! UINavigationController
//            let destanationVC: FilterViewController  = destanationNavVC.viewControllers.first as! FilterViewController
//            destanationVC.awakeFromStoryboard(parentView: self.viewController)
            let destanationVC: CoinsFilterViewController = segue.destination as! CoinsFilterViewController
            destanationVC.configuarateController(withPresenter: viewController.presenter)
            break
        default:
            break
        }
        // prepare here some data for destination viewController
        
        
    }
}
