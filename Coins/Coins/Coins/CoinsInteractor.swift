//
//  CoinsInteractor.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation

class CoinsInteractor: CoinsInteractorProtocol {
   
    
     weak var presenter: CoinsPresenterProtocol!
     var limit: Int = 30
    required init(presenter: CoinsPresenterProtocol) {
        self.presenter = presenter
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(changeInternetConection),
            name: Notification.Name(DataController.sharedDataController.internetConetionChangetNotification),
            object: nil
        )
    }
    
    func loadCoinsList(startIndex: Int, limit: Int, completition: @escaping (Array<CoinViewModel>?) -> Void) {
        DataController.sharedDataController.getCoinsList(first: startIndex) { (result) in
            completition(result)
        }
    }
    @objc func changeInternetConection()  {
        var i = 0
        presenter.reloadContextAfterConnection()
    }
    
    func setList(limit: Int) {
        self.limit = limit
    }
    
    func setCryptocurrncy(type: String) {
        DataController.sharedDataController.set(cryptocurrencyType: type)
    }
}
