//
//  CoinsDetailProtocols.swift
//  Coins
//
//  Created by Алексей Петров on 30/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

protocol CoinsDetailViewProtocol: class {
    func setTitleName()
}

protocol CoinsDetailPresenterProtocol: class {
    var router: CoinsDetailRouterProtocol! { set get }
    func configureView()
    func configureCell(cell: UITableViewCell, index: IndexPath) -> UITableViewCell
    func setNumbersOfRows() -> Int
    func setTitleText() -> String
    func startWith(coin: Coin) 
}

protocol CoinsDetailInteractorProtocol: class {
    
    
}

protocol CoinsDetailRouterProtocol: class {
    
}

protocol CoinsDetailConfiguratorProtocol: class {
      func configure(with viewController: CoinsDetailViewController)
}
