//
//  CoinCoreDataModel.swift
//  Coins
//
//  Created by Алексей Петров on 29/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import CoreData

struct CoinCoreDataModel {
    var name: String!
    var id: Double!
    var symbol: String!
    var slug: String!
    var cmcRank: Double!
    var numMarketPairs: Double!
    var circlatingSupply: Double!
    var totalSupply: Double!
    var lastUpdated: String!
    var dateAdded: String!
    var tags: Array<String>!
    var price: Double!
    var oneHourDynamic: Double!
    var oneDayDynamic: Double!
    var oneWeekDynamic: Double!
    var volumeDay: Double!
    var lastUpdate: String!
    var marketCap: Double!
    
    enum Keys: String {
        typealias RawValue = String
        case name
        case id
        case symbol
        case quote
        case slug
        case tags
        case cmcRank
        case numMarketPairs
        case circlatingSupply
        case totalSupply
        case lastUpdated
        case dateAdded
        case price
        case oneHourDynamic
        case oneDayDynamic
        case oneWeekDynamic
        case volumeDay
        case lastUpdate
        case marketCap
    }
    
    init(coin: Coin!) {
        name = coin.name
        id = coin.id
        symbol = coin.symbol
        slug = coin.slug
        cmcRank = coin.cmcRank
        numMarketPairs = coin.numMarketPairs
        circlatingSupply = coin.circlatingSupply
        totalSupply = coin.totalSupply
        lastUpdate = coin.quote.unatetStateDollarMetaInfo.lastUpdate
        lastUpdated = coin.lastUpdated
        dateAdded = coin.dateAdded
        tags = coin.tags
        price = coin.quote.unatetStateDollarMetaInfo.price
        oneDayDynamic = coin.quote.unatetStateDollarMetaInfo.oneDayDynamic
        oneHourDynamic = coin.quote.unatetStateDollarMetaInfo.oneHourDynamic
        oneWeekDynamic = coin.quote.unatetStateDollarMetaInfo.oneWeekDynamic
        volumeDay = coin.quote.unatetStateDollarMetaInfo.volumeDay
        marketCap = coin.quote.unatetStateDollarMetaInfo.marketCap
    }
    
    init(coin: CoinData!) {
        name = coin.name
        id = coin.id
        name = coin.name
        id = coin.id
        symbol = coin.symbol
        slug = coin.slug
        cmcRank = coin.cmcRank
        numMarketPairs = coin.numMarketPairs
        circlatingSupply = coin.circlatingSupply
        totalSupply = coin.totalSupply
        lastUpdate = coin.lastUpdate
        lastUpdated = coin.lastUpdated
        dateAdded = coin.dateAdded
        tags = coin.tags as? Array<String>
        price = coin.price
        oneWeekDynamic = coin.oneWeekDynamic
        oneHourDynamic = coin.oneHourDynamic
        oneDayDynamic = coin.oneDayDynamic
        volumeDay = coin.volumeDay
        marketCap = coin.marketCap
    }
    
    func createFromCoinCoreDataModel(newCoinRecord: NSManagedObject!) -> NSManagedObject! {
        newCoinRecord.setValue(name, forKey: Keys.name.rawValue)
        newCoinRecord.setValue(id, forKey: Keys.id.rawValue)
        newCoinRecord.setValue(symbol, forKey: Keys.symbol.rawValue)
        newCoinRecord.setValue(slug, forKey: Keys.slug.rawValue)
        newCoinRecord.setValue(cmcRank, forKey: Keys.cmcRank.rawValue)
        newCoinRecord.setValue(numMarketPairs, forKey: Keys.numMarketPairs.rawValue)
        newCoinRecord.setValue(circlatingSupply, forKey: Keys.circlatingSupply.rawValue)
        newCoinRecord.setValue(totalSupply, forKey: Keys.totalSupply.rawValue)
        newCoinRecord.setValue(lastUpdated, forKey: Keys.lastUpdated.rawValue)
        newCoinRecord.setValue(lastUpdate, forKey: Keys.lastUpdate.rawValue)
        newCoinRecord.setValue(dateAdded, forKey: Keys.dateAdded.rawValue)
        newCoinRecord.setValue(tags, forKey: Keys.tags.rawValue)
        newCoinRecord.setValue(price, forKey: Keys.price.rawValue)
        newCoinRecord.setValue(oneDayDynamic, forKey: Keys.oneDayDynamic.rawValue)
        newCoinRecord.setValue(oneHourDynamic, forKey: Keys.oneHourDynamic.rawValue)
        newCoinRecord.setValue(oneWeekDynamic, forKey: Keys.oneWeekDynamic.rawValue)
        newCoinRecord.setValue(volumeDay, forKey: Keys.volumeDay.rawValue)
        newCoinRecord.setValue(marketCap, forKey: Keys.marketCap.rawValue)
        return newCoinRecord
    }
}
