//
//  String+Util.swift
//  Coins
//
//  Created by Алексей Петров on 24/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation


extension String {
    static func emptyToNil(_ optionalString: String?) -> String {
        let text: String
        if let unwrapped = optionalString {
            text = unwrapped
        } else {
            text = ""
        }
        return text
    }
    
    static func doubleToString(double: Double) -> String {
        return String(format: "%.2f", double)
    }
}
