//
//  GlobalInfoInteractor.swift
//  Coins
//
//  Created by Алексей Петров on 08/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class GlobalInfoIteractor: GlobalInfoInteractorProtocol {
    weak var presenter: GlobalInfoPresenterProtocol!
    
    required init(presenter: GlobalInfoPresenterProtocol) {
        self.presenter = presenter
    }
    
    func loadGlobalInfo() {
//        DataController.sharedDataController.getGlobalInfo { (globalInfo) in
//            self.presenter.showGlobalInfo(globalInfo: (globalInfo)!)
//        }
    }
}
