//
//  Quote.swift
//  Coins
//
//  Created by Алексей Петров on 04/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation

struct Quote: Codable {
    var unatetStateDollarMetaInfo: USD
//    var bitcoinMetaInfo: BTC?
    
    enum CodingKeys: String, CodingKey {
        case USD
//        case BTC
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        unatetStateDollarMetaInfo = try container.decode(USD.self, forKey: .USD)
//        bitcoinMetaInfo = try container.decode(BTC.self, forKey: .BTC)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(unatetStateDollarMetaInfo, forKey: .USD)
//        try container.encode(bitcoinMetaInfo, forKey: .BTC)
    }
}

struct BTC: Codable {
    var price: Double?
    var oneHourDynamic: Double?
    var oneDayDynamic: Double?
    var oneWeekDynamic: Double?
    var volumeDay: Double?
    var lastUpdate: String
    var totalVolumeDay: Double?
    var totalMarketCap: Double?
    var marketCap: Double?
    
    enum CodingKeys: String, CodingKey {
        case price
        case oneHourDynamic = "percent_change_1h"
        case oneDayDynamic = "percent_change_24h"
        case oneWeekDynamic = "percent_change_7d"
        case volumeDay = "volume_24h"
        case lastUpdate = "last_updated"
        case marketCap = "market_cap"
        case totalMarketCap = "total_market_cap"
        case totalVolumeDay = "total_volume_24h"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        price = try container.decode(Double.self, forKey: .price)
        oneHourDynamic = try container.decode(Double.self, forKey: .oneHourDynamic)
        oneDayDynamic = try container.decode(Double.self, forKey: .oneDayDynamic)
        oneWeekDynamic = try container.decode(Double.self, forKey: .oneWeekDynamic)
        volumeDay = try container.decode(Double.self, forKey: .volumeDay)
        lastUpdate = try container.decode(String.self, forKey: .lastUpdate)
        marketCap = try container.decode(Double.self, forKey: .marketCap)
        totalMarketCap = try container.decode(Double.self, forKey: .totalMarketCap)
        totalVolumeDay = try container.decode(Double.self, forKey: .totalVolumeDay)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(price, forKey: .price)
        try container.encode(oneHourDynamic, forKey: .oneHourDynamic)
        try container.encode(oneDayDynamic, forKey: .oneDayDynamic)
        try container.encode(oneWeekDynamic, forKey: .oneWeekDynamic)
        try container.encode(volumeDay, forKey: .volumeDay)
        try container.encode(lastUpdate, forKey: .lastUpdate)
        try container.encode(marketCap, forKey: .marketCap)
        try container.encode(totalVolumeDay, forKey: .totalVolumeDay)
        try container.encode(totalMarketCap, forKey: .totalMarketCap)
    }
}

struct USD: Codable {
    var price: Double?
    var oneHourDynamic: Double?
    var oneDayDynamic: Double?
    var oneWeekDynamic: Double?
    var volumeDay: Double?
    var lastUpdate: String
    var marketCap: Double?
//    var totalVolumeDay: Double?
//    var totalMarketCap: Double?
    
    enum CodingKeys: String, CodingKey {
        case price
        case oneHourDynamic = "percent_change_1h"
        case oneDayDynamic = "percent_change_24h"
        case oneWeekDynamic = "percent_change_7d"
        case volumeDay = "volume_24h"
        case lastUpdate = "last_updated"
        case marketCap = "market_cap"
//        case totalMarketCap = "total_market_cap"
//        case totalVolumeDay = "total_volume_24h"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        price = try container.decode(Double.self, forKey: .price)
        oneHourDynamic = try container.decode(Double.self, forKey: .oneHourDynamic)
        oneDayDynamic = try container.decode(Double.self, forKey: .oneDayDynamic)
        oneWeekDynamic = try container.decode(Double.self, forKey: .oneWeekDynamic)
        volumeDay = try container.decode(Double.self, forKey: .volumeDay)
        lastUpdate = try container.decode(String.self, forKey: .lastUpdate)
        marketCap = try container.decode(Double.self, forKey: .marketCap)
//        totalMarketCap = try container.decode(Double.self, forKey: .totalMarketCap)
//        totalVolumeDay = try container.decode(Double.self, forKey: .totalVolumeDay)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(price, forKey: .price)
        try container.encode(oneHourDynamic, forKey: .oneHourDynamic)
        try container.encode(oneDayDynamic, forKey: .oneDayDynamic)
        try container.encode(oneWeekDynamic, forKey: .oneWeekDynamic)
        try container.encode(volumeDay, forKey: .volumeDay)
        try container.encode(lastUpdate, forKey: .lastUpdate)
        try container.encode(marketCap, forKey: .marketCap)
//        try container.encode(totalVolumeDay, forKey: .totalVolumeDay)
//        try container.encode(totalMarketCap, forKey: .totalMarketCap)
    }
}
