//
//  CoinsDetailPresenter.swift
//  Coins
//
//  Created by Алексей Петров on 30/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit


class CoinsDetailPresenter: CoinsDetailPresenterProtocol {
    
    
    weak var view: CoinsDetailViewProtocol!
    var interactor: CoinsDetailInteractorProtocol!
//    var coin: Coin = Coin.init()
    var router: CoinsDetailRouterProtocol!
    private let numberOfRows: Int = 5
    let indeficators: Array = ["timeUpdateCell","priceCell", "changeCell", "dayChangeCell", "weekChangeCell"]
    required init(view: CoinsDetailViewProtocol) {
        self.view = view
    }
    
    func configureView() {
        view?.setTitleName()
    }
    
    func configureCell(cell: UITableViewCell, index: IndexPath) -> UITableViewCell {
//        switch index.row {
//        case 0:
//            DataController.sharedDataController.dataFormatter.dateFormat = "HH:mm d MMMM"
////            let resultDate = DataController.sharedDataController.dataFormatter.string(from: coin.lastUpdate)
//            cell.textLabel?.text = String(format: "Last update: %@", resultDate)
//            
//            return cell
//        case 1 :
//            cell.textLabel?.text = "price:"
////            cell.detailTextLabel?.text = String(format: "$ %@", coin.price)
//            return cell
//        case 2 :
//            if coin.quote.oneHourDynamicsNum < 0.0 {
//                cell.detailTextLabel?.textColor = UIColor.red
//            } else {
//                cell.detailTextLabel?.textColor = UIColor.green
//            }
//            cell.textLabel?.text = "recent hour dynamics:"
//            cell.detailTextLabel?.text = String(format: "%@%%", coin.quote.oneHourDynamics)// coin.quote.oneHourDynamics
//            return cell
//        case 3 :
//            if coin.quote.oneDayDynamicsNum < 0.0 {
//                cell.detailTextLabel?.textColor = UIColor.red
//            } else {
//                cell.detailTextLabel?.textColor = UIColor.green
//            }
//            cell.textLabel?.text = "recent day dynamics:"
//            cell.detailTextLabel?.text = String(format: "%@%%", coin.quote.oneDayDynamics) //coin.quote.oneDayDynamics
//            return cell
//        case 4 :
//            cell.textLabel?.text = "recent week dynamics:"
//            if coin.quote.oneWeekDynamicsNum < 0.0 {
//                cell.detailTextLabel?.textColor = UIColor.red
//            } else {
//                cell.detailTextLabel?.textColor = UIColor.green
//            }
//            cell.detailTextLabel?.text = String(format: "%@%%", coin.quote.oneWeekDynamics)
//            return cell
//        case 5 :
//            cell.textLabel?.text = "tags:"
//            cell.detailTextLabel?.text = "new, lastes, mineable, oder_fuckin_tags, i_don't_know, super, part_2"
//            return cell
//        default:
//            return cell
//        }
//        return cell
        return cell
    }
    
    func setNumbersOfRows() -> Int {
        return numberOfRows
    }
    
    func setTitleText() -> String {
        return "" //  coin.name as String
    }
    
    func startWith(coin: Coin) {
//        self.coin = coin
    }
    
}
