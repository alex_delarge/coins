//
//  FilterRouter.swift
//  Coins
//
//  Created by Алексей Петров on 04/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class FilterRouter: FilterRouterProtocol {
    weak var viewController: FilterViewController!
    weak var delegate: CoinsViewControllerDelegate?
    
//    private var coin: Coin = Coin.init()
    init(viewController: FilterViewController) {
        self.viewController = viewController
    }
    
    func closeView() {
        self.viewController.dismiss(animated: true, completion: nil)
    }
    
    func  delegate(object: AnyObject) {
        delegate = object as? CoinsViewControllerDelegate
    }
    
    func applyChanges () {
        self.closeView()
    }
//   func  prepare(for segue: UIStoryboardSegue, sender: Any?)  {
//
//    }
}
