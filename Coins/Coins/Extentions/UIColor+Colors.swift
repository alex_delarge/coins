//
//  UIImageview+Colors.swift
//  Coins
//
//  Created by Алексей Петров on 14/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
     // MARK: - ...
    static var growthColor: UIColor {
//        return #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
        return UIColor.init(red: 81.0/255.0, green: 177.0/255.0, blue: 56.0/255.0, alpha: 1.0)
    }
    
    static var downColor: UIColor {
//        return #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        return UIColor.init(red: 255.0/255.0, green: 45.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    }
    
    static var defaultButtonColor: UIColor {
        return UIColor.init(red: 90.0/255.0, green: 112.0/255.0, blue: 135.0/255.0, alpha: 1.0)
    }
    
    
    /**
     ###Title
     
     ###parameters:
     - parameter: description
     
     ###Returns:
     - returnet value: discription
     
     ### Example: ###
     ````
     Some code
     
     ````
     
     */
    static var inactiveButtonColor: UIColor {
        return UIColor.white
    }
}
