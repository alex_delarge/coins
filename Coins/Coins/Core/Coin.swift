//
//  Coin.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation

struct Coin: Codable {
    var name: String
    var id: Double
    var symbol: String
    var quote: Quote
    var slug: String
    var cmcRank: Double
    var numMarketPairs: Double
    var circlatingSupply: Double
    var totalSupply: Double
//    var maxSupply: Double!
    var lastUpdated: String
    var dateAdded: String
    var tags: Array<String>
    

    enum CodingKeys: String, CodingKey {
        case name
        case id
        case symbol
        case quote
        case slug
        case tags
        case cmcRank = "cmc_rank"
        case numMarketPaits = "num_market_pairs"
        case circlatingSupply = "circulating_supply"
        case totalSupply = "total_supply"
//        case maxSupply = "max_supply"
        case lastUpdated = "last_updated"
        case dateAdded = "date_added"
    }
    
    init(from decoder: Decoder) throws {
        let contanier = try decoder.container(keyedBy: CodingKeys.self)
        name = try contanier.decode(String.self, forKey: .name)
        id = try contanier.decode(Double.self, forKey: .id)
        symbol = try contanier.decode(String.self, forKey: .symbol)
        quote = try contanier.decode(Quote.self, forKey: .quote)
        slug = try contanier.decode(String.self, forKey: .slug)
        cmcRank = try contanier.decode(Double.self, forKey: .cmcRank)
        numMarketPairs = try contanier.decode(Double.self, forKey: .numMarketPaits)
        circlatingSupply = try contanier.decode(Double.self, forKey: .circlatingSupply)
        totalSupply = try contanier.decode(Double.self, forKey: .totalSupply)
//        maxSupply = try contanier.decode(Double.self, forKey: .maxSupply)
        lastUpdated = try contanier.decode(String.self, forKey: .lastUpdated)
        dateAdded = try contanier.decode(String.self, forKey: .dateAdded)
        tags = try contanier.decode([String].self, forKey: .tags)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(id, forKey: .id)
        try container.encode(symbol, forKey: .symbol)
        try container.encode(quote, forKey: .quote)
        try container.encode(slug, forKey: .slug)
        try container.encode(cmcRank, forKey: .cmcRank)
        try container.encode(numMarketPairs, forKey: .numMarketPaits)
        try container.encode(circlatingSupply, forKey: .circlatingSupply)
        try container.encode(totalSupply, forKey: .totalSupply)
        try container.encode(lastUpdated, forKey: .lastUpdated)
        try container.encode(dateAdded, forKey: .dateAdded)
        try container.encode(tags, forKey: .tags)
//        try container.encode(maxSupply, forKey: .maxSupply)
        
    }
}

struct CoinsListResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
    }
    let data: [Coin]
}
