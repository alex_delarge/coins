//
//  FilterProtocol.swift
//  Coins
//
//  Created by Алексей Петров on 04/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

protocol FilterViewProtocol: class {
    func setSlider(count: Int)
    func setQuantityLabel(text: String)
}

protocol FilterPresenterProtocol: class {
    var router: FilterRouterProtocol! { set get }
    func configureView()
    func configureCell(cell: FilterTableViewCell, index: IndexPath) -> UITableViewCell
    func setNumbersOfRows() -> Int
    func closeFilterView()
    func change(count: Int)
    func sectionTitle() -> String
    func applyChanges()
    func selectFilterAt(index: IndexPath)
    
  
}

protocol FilterInteractorProtocol: class {
  func setDownloadsCount(limit: Int)
    func set(sort: String)
}

protocol FilterRouterProtocol: class {
    func closeView()
    func applyChanges ()
    func  delegate(object: AnyObject)
//    func prepare(for segue: UIStoryboardSegue, sender: Any?) 

}

protocol FilterConfiguratorProtocol: class {
    func configure(with viewController: FilterViewController)
    
    
}
