//
//  CoinsViewController.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinsViewController: UIViewController, CoinsViewProtocol, CoinsViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    //MARK: view elements
    @IBOutlet weak var favoritesViewConstrait: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backViewWidthConstrait: NSLayoutConstraint!
    
    //MARK:  buttons
    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var coinsButton: UIButton!
    @IBOutlet weak var tokensButton: UIButton!
    
    //MARK: Privates
    private let backViewAllWidght: Double = 70
    private let backViewCoinsWidght: Double = 77
    private let backViewTokensWigght: Double = 90
    let resorceIndef: String = "CoinTablevVewCell"
    let detailSegue = "CoinsToDetail"
    let filterSegue = "filterSegue"
    var presenter: CoinsPresenterProtocol!
    let configurator: CoinsConfiguratorProtocol = CoinsConfigurator()
    @IBOutlet weak var internetConnectionStatus: UIView!
    
    var viewFrame: CGRect {
        return self.view.frame
    }
    
    override func viewDidLoad() {
         super.viewDidLoad()
        tableView.register(UINib(nibName: "CoinTablevVewCell", bundle: nil), forCellReuseIdentifier: "CoinTablevVewCell")
        configurator.configure(with: self)
         presenter.configureView()
    }
    
 
    // MARK: Actions
    @IBAction func tokensButtonCliked(_ sender: UIButton) {
        sender.tintColor = UIColor.inactiveButtonColor
        allButton.tintColor = UIColor.defaultButtonColor
        coinsButton.tintColor = UIColor.defaultButtonColor
        presenter.setCryprocyrrency(type: "tokens")
        presenter.changeBackView(center: sender.center, widght: backViewTokensWigght)
    }
    
    @IBAction func allButtonCliked(_ sender: UIButton) {
        tokensButton.tintColor = UIColor.defaultButtonColor
        sender.tintColor = UIColor.inactiveButtonColor
        coinsButton.tintColor = UIColor.defaultButtonColor
        presenter.setCryprocyrrency(type: "all")
        presenter.changeBackView(center: sender.center, widght: backViewAllWidght)
    }
    
    @IBAction func coinsButtonCliked(_ sender: UIButton) {
        tokensButton.tintColor = UIColor.defaultButtonColor
        allButton.tintColor = UIColor.defaultButtonColor
        sender.tintColor = UIColor.inactiveButtonColor
        presenter.setCryprocyrrency(type: "coins")
        presenter.changeBackView(center: sender.center, widght: backViewCoinsWidght)
    }
    
    @IBAction func filterButtonCliked(_ sender: UIBarButtonItem) {
        presenter.filterViewOpen()
    }
    
    @IBAction func showFavorites(_ sender: Any) {
        presenter.showFavorites()
    }
    
    // MARK: UITableView - protocol methods
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.setNumbersOfRows()
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CoinTablevVewCell = tableView .dequeueReusableCell(withIdentifier: resorceIndef, for: indexPath) as! CoinTablevVewCell
        return  presenter.configureCell(cell: cell, index: indexPath)
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        presenter.fetchData(index: indexPath)
    }
    
    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        let cell: CoinTablevVewCell = tableView.cellForRow(at: indexPath) as! CoinTablevVewCell
////         debugPrint(cell.isSelected)
//        if cell.isSelected {
//            tableView.deselectRow(at: indexPath, animated: true)
//            tableView.delegate?.tableView!(tableView, didDeselectRowAt: indexPath)
//        }
//        return indexPath
//    }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {

        return indexPath
    }
    
     func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
         let cell: CoinTablevVewCell = tableView.cellForRow(at: indexPath) as! CoinTablevVewCell

    
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.getCellSize(indexPath: indexPath)
    }
    
    //MARK: CoinsViewController protocol methods
    
    func startReloadDataInTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func setCreateUITableViewCell(cell: UITableViewCell) {
        
    }
    
    func changeBackView(center: CGPoint, widght: Double) {
        view.layoutIfNeeded()
//        backViewWidthConstrait.constant = CGFloat(widght)
        UIView.animate(withDuration: UIView.defaultDuration) {
            let size: CGSize = CGSize(width: widght, height: 25)
            self.backView.frame.size = size
            self.backView.center = center
            self.view.layoutIfNeeded()
        }
    }
    func fetchCoinsToList(indexOfLastCoin: Int) {
//        self.tableView.insertRows(at: [NSIndexPath(row: indexOfLastCoin + 1, section: 0) as IndexPath], with: UITableView.RowAnimation.automatic)
          self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.router.prepare(for: segue, sender: sender)
    }
    
    func didFinishTask() {
        presenter.reloadContext()
    }
    
    func showInternetConnectionStatus(show: Bool) {
//        self.internetConnectionStatus.isHidden = !show
    }
    
    func hideTableView(hide: Bool) {
        self.tableView.isHidden = hide
    }
    
    func startingButtonsState()  {
        tokensButton.tintColor = UIColor.defaultButtonColor
        allButton.tintColor = UIColor.inactiveButtonColor
        coinsButton.tintColor = UIColor.defaultButtonColor
        tableView.contentOffset.y = view.frame.height
    }
    
    func showTableAutomaticly() {
        UIView.animate(withDuration: UIView.defaultDuration) {
            self.tableView.contentOffset.y = 0.0
        }
    }
    
    func setupNavigationBar() {
//        collectionView.register(UINib(nibName: "CoinsHeaderMenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CoinsHeaderMenuCollectionViewCell")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
    }
    
    func stopShowingFavorites() {
        view.layoutIfNeeded()
        favoritesViewConstrait.constant = presenter.favoritesViewHidenHight
        UIView.animate(withDuration: UIView.defaultDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func startShowingFavorites() {
        view.layoutIfNeeded()
        favoritesViewConstrait.constant = presenter.favoritesViewShowHight
        UIView.animate(withDuration: UIView.defaultDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func setCollectionViewCell(frame: CGRect) {
    }
    
    
    func setCellSize(indexPath: IndexPath) -> CGFloat {
//        if (tableView.cellForRow(at: indexPath) != nil) {
//            let cell: CoinTablevVewCell = tableView.cellForRow(at: indexPath) as! CoinTablevVewCell
//            if cell.isSelected {
//                return 400.0
//            }
//            else if !cell.isSelected {
//                return UITableView.automaticDimension
//            }
//
//        }
         return UITableView.automaticDimension
    }
    
    func hideInfo(atIndexPath: IndexPath) {
//        self.tableView.reloadRows(at: [atIndexPath], with: .automatic)
        let cell: CoinTablevVewCell = tableView.cellForRow(at: atIndexPath) as! CoinTablevVewCell
//        cell.isSelected = false
        cell.changeState(isSelect: false) {
            self.tableView.reloadRows(at: [atIndexPath], with: .automatic)

        }
    }
    
    func reloadRow(atIndexPath: IndexPath) {
        self.tableView.reloadRows(at: [atIndexPath], with: .automatic)
    }
}
