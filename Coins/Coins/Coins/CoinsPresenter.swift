//
//  CoinsPresenter.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinsPresenter: CoinsPresenterProtocol {

    //MARK: private lets and vars
    private let startIndex: Int = 1
    private var selectedIdex: IndexPath? = nil
    private  let limit: Int = 30
    
    weak var view: CoinsViewProtocol!
    var tableView: UITableView
    var interactor: CoinsInteractorProtocol!
    var router: CoinsRouterProtocol!
    private var dataArray: Array<CoinViewModel>
    private var isShowFavorites: Bool = false
    
    required init(view: CoinsViewProtocol) {
        self.view = view
        tableView = UITableView.init()
        self.dataArray = Array()
    }
    
    func configureView() {
        self.view.startingButtonsState()
        interactor.loadCoinsList(startIndex: self.startIndex, limit: self.limit) { (result) in
            self.dataArray = result!
            self.view.setupNavigationBar()
            self.view?.startReloadDataInTableView()
            self.view.showTableAutomaticly()
            if !(DataController.sharedDataController.checkInternet()) {
                self.view.showInternetConnectionStatus(show: true)
            
            }
        }
        
    }
    
    func reloadContext()  {
        interactor.loadCoinsList(startIndex: startIndex, limit: limit) { (result) in
            self.dataArray = result!
            self.view?.startReloadDataInTableView()
        }
        
    }
    
    func reloadContextAfterConnection () {

        if (DataController.sharedDataController.checkInternet()) {
            self.view.showInternetConnectionStatus(show: false)
            interactor.loadCoinsList(startIndex: 1, limit: 30) { (result) in
                self.dataArray = result!
//                 self.view?.hideTableView(hide: false)
                self.view?.startReloadDataInTableView()
            }
        } else {
            self.view.showInternetConnectionStatus(show: true)
        }
    }
    
    func configureCell(cell: CoinTablevVewCell, index: IndexPath) -> UITableViewCell {
        let coin: CoinViewModel = dataArray[index.row]
        cell.nameLabel.text = coin.name
        cell.priceLabel.text = "$ " + coin.price!
        cell.growthLabel.text = String(format: "%@%%", coin.change!)
        cell.marketCapLabel.text = "$ " + coin.marketCup!
        cell.setupGrowthLabel(number: coin.numChange!)
        cell.symbolLabel.text = coin.symbol
//        cell.circlatingSupplyLabel.text = coin.circlatingSupply
        cell.totalSupplyLabel.text = coin.totalSupply
        cell.volumeDayLabel.text = coin.volumeDay
        if index == selectedIdex {
            cell.setLargeState()
            cell.setupAdditionalDynamicDay(number: coin.numChangeDay!)
            cell.setupAdditionalDynamicWeek(number: coin.numChangeWeek!)
        } else {
            cell.setSmallState()
        }

        return cell
    }
    
    func setNumbersOfRows() -> Int {
        if self.dataArray.count > 0 {
            return self.dataArray.count
        }
        return 0
    }
    
    func fetchData(index: IndexPath) {

        if (index.row == dataArray.count - 1) {
        interactor.loadCoinsList(startIndex: index.row, limit: self.limit) { (result) in
            var resultArray: Array = self.dataArray as Array
            resultArray = resultArray + (result! as [CoinViewModel])
            self.dataArray = resultArray
            self.view?.startReloadDataInTableView()
            debugPrint(self.dataArray.count)
        }
        }
    }
    
    func didSelectRow(indexPath: IndexPath) {
        if selectedIdex == indexPath {
            selectedIdex = nil
            view.reloadRow(atIndexPath: indexPath)
        } else {
            let oldIndex = selectedIdex
            selectedIdex = indexPath
            if oldIndex != nil {
                view .reloadRow(atIndexPath: oldIndex!)
            }
            view.reloadRow(atIndexPath: indexPath)
        }
    }
    
    func didDeselectRow(indexPath: IndexPath) {
        
    }
    
    func filterViewOpen() {
        router.showFilter()
    }
    
    
    func setCryprocyrrency(type: String) {
        self.interactor.setCryptocurrncy(type: type)
        dataArray = Array()
        interactor.loadCoinsList(startIndex: startIndex, limit: limit) { (result) in
            var resultArray: Array = self.dataArray
            resultArray = resultArray + (result! as [CoinViewModel])
            self.dataArray = resultArray
            self.selectedIdex = nil
            self.view?.startReloadDataInTableView()
        }
        self.view?.startReloadDataInTableView()
    }
    
    func confifCollectionCell(cell: CoinsHeaderMenuCollectionViewCell , indexPath: IndexPath) -> CoinsHeaderMenuCollectionViewCell {
//        cell.frame.size.width = self.view.viewFrame.size.width - 150
//        debugPrint(self.view.viewFrame.size.width - 150)

//        if indexPath.section == 0 {
//
//        }
        return cell
    }
    
    func confifCollectionCell(cell: UICollectionViewCell) -> UICollectionViewCell {
        return cell
    }
    
    func getCellsCount(section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return 4
    }
    
    func getSectionCount() -> Int {
        return 2
    }
    
    func showFavorites() {
        if isShowFavorites {
            view.stopShowingFavorites()
            isShowFavorites = false
        } else if !isShowFavorites {
            view.startShowingFavorites()
            isShowFavorites = true
        }
    }
    
    
    func getCellSize(indexPath: IndexPath) -> CGFloat {
        return view.setCellSize(indexPath:indexPath)
    }
    
    func changeBackView(center: CGPoint, widght: Double) {
        view.changeBackView(center: center, widght: widght)
    }
//    func getCellsSize() -> CGSize {
//        return CGSize(width: self.view.vi, height: <#T##CGFloat#>)
//    }
    
}
