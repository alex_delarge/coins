//
//  CoinPresenter+ Extentions.swift
//  Coins
//
//  Created by Алексей Петров on 26/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

extension CoinsPresenterProtocol {
    var favoritesViewHidenHight: CGFloat {
        return 54.0
    }
    
    var favoritesViewShowHight: CGFloat {
        return 250.0
    }
}
