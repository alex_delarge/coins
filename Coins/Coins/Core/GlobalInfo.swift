//
//  GlobalInfo.swift
//  Coins
//
//  Created by Алексей Петров on 08/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation


struct GlobalIngo: Codable {
    var btcDominance: Double
    var ethDominance: Double
    var activeCryptocurrencies: Double
    var activeMarketPairc: Double
    var activeExchanges: Double
    var lastUpdated: Double
    var quote: Quote
    
    enum CodingKeys: String, CodingKey {
        case quote
        case btcDominance = "btc_dominance"
        case ethDominance = "eth_dominance"
        case activeCryptocurrencies = "active_cryptocurrencies"
        case activeMarketPairc = "active_market_pairs"
        case activeExchanges = "active_exchanges"
        case lastUpdated = "last_updated"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        btcDominance = try container.decode(Double.self, forKey: .btcDominance)
        ethDominance = try container.decode(Double.self, forKey: .ethDominance)
        activeExchanges = try container.decode(Double.self, forKey: .activeExchanges)
        activeMarketPairc = try container.decode(Double.self, forKey: .activeMarketPairc)
        activeCryptocurrencies = try container.decode(Double.self, forKey: .activeCryptocurrencies)
        lastUpdated = try container.decode(Double.self, forKey: .lastUpdated)
        quote = try container.decode(Quote.self, forKey: .quote)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(btcDominance, forKey: .btcDominance)
        try container.encode(ethDominance, forKey: .ethDominance)
        try container.encode(activeCryptocurrencies, forKey: .activeCryptocurrencies)
        try container.encode(activeMarketPairc, forKey: .activeMarketPairc)
        try container.encode(activeExchanges, forKey: .activeExchanges)
        try container.encode(lastUpdated, forKey: .lastUpdated)
        try container.encode(quote, forKey: .quote)
    }
}

struct GlobalInfoResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
    }
    let data: [GlobalIngo]
}
