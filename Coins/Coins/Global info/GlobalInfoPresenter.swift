//
//  GlobalInfoPresenter.swift
//  Coins
//
//  Created by Алексей Петров on 08/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class GlobalInfoPresenter: GlobalInfoPresenterProtocol {
    
    var router: GlobalInfoRouterProtocol!
    var detailCellIndifaier: String = "detailCell"
    weak var view: GlobalInfoViewProtocol!
    var interactor: GlobalInfoInteractorProtocol!
//    var globalInfo: GlobalInfo!
    
    private var rowsCount = 0
   
    required init(view: GlobalInfoViewProtocol) {
        self.view = view
    }
    
    func configureView() {
        self.interactor.loadGlobalInfo()
    }
    
//    func showGlobalInfo(globalInfo: GlobalInfo)  {
//        self.globalInfo = globalInfo
//        self.view.reloadContext()
//        self.view.set(cryptocurrensyNumber: globalInfo.activeCryptocurrencies)
//        rowsCount = 3
//
//    }
    
    func numberOfCells () -> Int {
        return self.rowsCount
    }
    
    func configurate(cell: UITableViewCell, indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            cell.textLabel?.text = "Markets:"
//            cell.detailTextLabel?.text = globalInfo.activeMarketPairs
            return cell
        }
        if (indexPath.row == 1) {
            cell.textLabel?.text = "BTC Dominance:"
//            cell.detailTextLabel?.text = String(format: "%@ %%", globalInfo.btcDominance)
        }
        if (indexPath.row == 2) {
            cell.textLabel?.text = " Market Cap:"
//            cell.detailTextLabel?.text = String(format: "$ %@", globalInfo.totalMarketCap)
        }
        return cell
    }
}
