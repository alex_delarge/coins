//
//  UILabel+Animate.swift
//  Coins
//
//  Created by Алексей Петров on 02/02/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func animate(font: UIFont, duration: TimeInterval, completition:@escaping () -> Void) {
         let oldFrame = frame
        let labelScale = self.font.pointSize / font.pointSize
        self.font = font
        let oldTransform = transform
        transform = transform.scaledBy(x: labelScale, y: labelScale)
         let newOrigin = frame.origin
         frame.origin = oldFrame.origin // only for left aligned text
//         frame.origin = CGPoint(x: oldFrame.origin.x + oldFrame.width - frame.width, y: oldFrame.origin.y) // only for right aligned text
        setNeedsUpdateConstraints()
        UIView.animate(withDuration: duration) {
            self.frame.origin = newOrigin
            self.transform = oldTransform
            self.layoutIfNeeded()
            completition()
        }
    }
}
