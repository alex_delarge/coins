//
//  CoinViewModel.swift
//  Coins
//
//  Created by Алексей Петров on 24/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation

struct CoinViewModel {
    var name: String?
    var price: String?
    var change: String?
    var marketCup: String?
    var numChange: Double?
    var numChangeDay: Double?
    var numChangeWeek: Double?
    var symbol: String?
    var circlatingSupply: String?
    var totalSupply: String?
    var volumeDay: String?
    
    init(coin: Coin) {
        name = String.emptyToNil(coin.name)
        symbol = String.emptyToNil(coin.symbol)
        price = String.doubleToString(double: coin.quote.unatetStateDollarMetaInfo.price!)
        change = String.doubleToString(double: coin.quote.unatetStateDollarMetaInfo.oneHourDynamic!)
        marketCup = String.doubleToString(double: coin.quote.unatetStateDollarMetaInfo.marketCap! / 1000000000) + " bil."
        numChange = coin.quote.unatetStateDollarMetaInfo.oneHourDynamic!
        numChangeDay = coin.quote.unatetStateDollarMetaInfo.oneDayDynamic!
        numChangeWeek = coin.quote.unatetStateDollarMetaInfo.oneWeekDynamic!
        circlatingSupply = String.doubleToString(double: coin.circlatingSupply) + " ETH"
        totalSupply = String.doubleToString(double: coin.totalSupply) + " ETH"
        volumeDay = "$ " + String.doubleToString(double: coin.quote.unatetStateDollarMetaInfo.volumeDay! / 1000000000) + " bil."
        
    }
}
