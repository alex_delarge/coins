//
//  RequestManager.swift
//  Coins
//
//  Created by Алексей Петров on 25/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import Alamofire


class RequestManager {
    private let baseURL: String
    private let APIKey: String
    
    init() {
        self.baseURL = "https://pro-api.coinmarketcap.com/v1/"
        self.APIKey = "87b3156b-9a76-46a8-abf3-fca54fd26c64"
    }
    
    func request(request: String, parameters: Parameters!, completion: @escaping (_ result: Any?) -> Void)  {
        let requestString: String = self.baseURL + request
        let requestHeaders: HTTPHeaders = ["X-CMC_PRO_API_KEY": self.APIKey]
        Alamofire.request(requestString, method: .get , parameters: parameters, encoding: URLEncoding.default, headers: requestHeaders)
            .responseJSON { response  in
                if (response.result.isSuccess) {
                    completion(self.parseToDataItem(source: response.data, request: request))
                } else {
                    completion(nil)
                }
        }
    }

    private func parseToDataItem(source: Data!, request: String) -> Any! {
        switch request {
        case "cryptocurrency/listings/latest":
            do {
                let coins: CoinsListResponse = try JSONDecoder().decode(CoinsListResponse.self, from: source)
                return coins.data
            } catch let error {
                debugPrint(error.localizedDescription)
                return nil
            }
        case "global-metrics/quotes/latest" :
            do {
                let metrics: GlobalInfoResponse = try JSONDecoder().decode(GlobalInfoResponse.self, from: source)
                return metrics.data
            } catch let error {
                debugPrint(error.localizedDescription)
                return nil
            }
        default:
            return nil
        }
    }
}


