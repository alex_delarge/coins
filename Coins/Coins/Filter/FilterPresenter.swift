//
//  FilterPresenter.swift
//  Coins
//
//  Created by Алексей Петров on 04/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class FilterPresenter: FilterPresenterProtocol {
    
    var router: FilterRouterProtocol!
    let filters: Array = ["name", "symbol", "date_added", "market_cap", "price", "circulating_supply", "total_supply", "max_supply", "num_market_pairs", "volume_24h", "percent_change_1h", "percent_change_24h", "percent_change_7d"]
    weak var view: FilterViewProtocol!
    var interactor: FilterInteractorProtocol!
    var limit: Int = DataController.sharedDataController.limit
    
    required init(view: FilterViewProtocol) {
        self.view = view
    }
    
    func configureView() {
        view.setSlider(count: DataController.sharedDataController.limit)
        view.setQuantityLabel(text: String(format: "%i", DataController.sharedDataController.limit))
    }
    func configureCell(cell: FilterTableViewCell, index: IndexPath) -> UITableViewCell {
        cell.label.text = self.filters[index.row]
        return  cell
    }
    
    func setNumbersOfRows() -> Int {
        return filters.count
    }
    
    func closeFilterView() {
        router.closeView()
    }
    
    func change(count: Int) {
        self.limit = count
        interactor.setDownloadsCount(limit: count)
        view?.setQuantityLabel(text: String(format: "%i", count))
    }
    
    func selectFilterAt(index: IndexPath) {
        interactor.set(sort: filters[index.row])
    }
    
    func applyChanges() {
        router.applyChanges()
    }
    
    func setNumbersOfSection() -> Int {
        return 0
    }
    
    func sectionTitle() -> String {
        return "sort mods"
    }
}
