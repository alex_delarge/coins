//
//  FileViewController.swift
//  Coins
//
//  Created by Алексей Петров on 04/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class FilterViewController: UIViewController, FilterViewProtocol, UITableViewDelegate, UITableViewDataSource {
    let resourceIndifaer: String = "filterCell"
    var delegate: CoinsViewControllerDelegate?
    var presenter: FilterPresenterProtocol!
    let configurator: FilterConfiguratorProtocol = FilterConfigurator()
    
    @IBOutlet weak var countSlider: UISlider!
    @IBOutlet weak var quantityLabel: UILabel!
    
    @IBAction func didValueChanged(_ sender: UISlider) {
        presenter.change(count: Int(sender.value))
    }
    
    @IBAction func closeButtonCliked(_ sender: Any) {
        presenter.closeFilterView()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()
    }
    
    @IBAction func applyButtonCliked(_ sender: Any) {
        delegate?.didFinishTask()
        presenter.applyChanges()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.selectFilterAt(index: indexPath)
   
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Sort by:"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.setNumbersOfRows()
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FilterTableViewCell = tableView .dequeueReusableCell(withIdentifier: resourceIndifaer, for: indexPath) as! FilterTableViewCell
        return presenter.configureCell(cell: cell, index: indexPath)
    }
    
    func setSlider(count: Int) {
        countSlider.value = Float(count)
    }
    
    func setQuantityLabel(text: String) {
        quantityLabel.text = text
    }
    
    func awakeFromStoryboard(parentView: CoinsViewController) {
        configurator.configure(with: self)
//        presenter.configureView()
        self.delegate  = parentView
    }
    
//    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        <#code#>
//    }
}
