//
//  CoinsDetailConfigurator.swift
//  Coins
//
//  Created by Алексей Петров on 30/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinsDetailConfigurator: CoinsDetailConfiguratorProtocol {
    

    
    func configure(with viewController: CoinsDetailViewController) {
        let presenter = CoinsDetailPresenter(view: viewController)
        let interactor = CoinsDetailInteractor(presenter: presenter)
        let router = CoinsDetailRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
