//
//  FilterViewController.swift
//  Coins
//
//  Created by Алексей Петров on 11/02/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinsFilterViewController: UIViewController {
    var presenter: CoinsPresenterProtocol!
    
    func configuarateController(withPresenter: CoinsPresenterProtocol!) {
        presenter = withPresenter
    }
    
    override func viewDidLoad() {
        
        
    }
    
    
    @IBAction func tapOnClose(_ sender: Any) {
        self .dismiss(animated: true, completion: nil)
    }
}
