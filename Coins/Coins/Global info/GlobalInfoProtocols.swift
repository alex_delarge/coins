//
//  GlobalInfoProtocols.swift
//  Coins
//
//  Created by Алексей Петров on 08/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.

import Foundation
import UIKit

protocol GlobalInfoViewProtocol: class {
    func reloadContext()
    func set(cryptocurrensyNumber: String)
}

protocol GlobalInfoPresenterProtocol: class {
    var router: GlobalInfoRouterProtocol! { set get }
    var detailCellIndifaier: String { get }
    func configureView()
//    func showGlobalInfo(globalInfo: GlobalInfo)
    func configurate(cell: UITableViewCell, indexPath: IndexPath) -> UITableViewCell
    func numberOfCells () -> Int
    
}

protocol GlobalInfoInteractorProtocol: class {
    func loadGlobalInfo()
}

protocol GlobalInfoRouterProtocol: class {
    
}

protocol GlobalInfoConfiguratorProtocol: class {
    func configure(with viewController: GlobalInfoViewController)
}


