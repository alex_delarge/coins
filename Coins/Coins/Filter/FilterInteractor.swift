//
//  FilterInteractor.swift
//  Coins
//
//  Created by Алексей Петров on 04/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation

class FilterInteractor: FilterInteractorProtocol {
    
    weak var presenter: FilterPresenterProtocol!
    weak var parentIterator: CoinsInteractorProtocol!
    
    required init(presenter: FilterPresenterProtocol) {
        self.presenter = presenter
    }
    
    func setDownloadsCount(limit: Int) {
        DataController.sharedDataController.set(limit: limit)
    }
    
    func set(sort: String) {
        DataController.sharedDataController.set(sort: sort)
    }
    
}
