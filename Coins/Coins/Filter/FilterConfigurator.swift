//
//  FilterCreator.swift
//  Coins
//
//  Created by Алексей Петров on 04/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation

class FilterConfigurator: FilterConfiguratorProtocol {
    
    func configure(with viewController: FilterViewController) {
        let presenter = FilterPresenter(view: viewController)
        let interactor = FilterInteractor(presenter: presenter)
        let router = FilterRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
