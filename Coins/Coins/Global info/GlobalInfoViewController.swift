//
//  GlobalInfoViewController.swift
//  Coins
//
//  Created by Алексей Петров on 08/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class GlobalInfoViewController: UIViewController, GlobalInfoViewProtocol, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cryptoLabel: UILabel!
    
    var presenter: GlobalInfoPresenterProtocol!
    let configurator: GlobalInfoConfiguratorProtocol = GlobalInfoConfigurator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        presenter.configureView()
    }
    
    // UItableview Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfCells()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: presenter.detailCellIndifaier, for: indexPath)
        return presenter.configurate(cell: cell, indexPath: indexPath)
    }
    
    // GlobalInfoViewPrrotocol
    
    func reloadContext() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func set(cryptocurrensyNumber: String) {
        DispatchQueue.main.async {
            self.cryptoLabel.text = cryptocurrensyNumber
        }
    }
}
