//
//  DataController.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//



import Foundation
import Alamofire
import SystemConfiguration
import Reachability

/// ##Класс для загрузки данных из сети или с диска устройства

class DataController {
    
    /// ###Адаптер для Alamofire
    private var requestManager: RequestManager
    
    /// ###Реализация работы с Core Data
    private let coreDataInterface: CoreDataInterface
    
    
    private var reachability: Reachability!
    
    /// ###Лимит размера листа криптовалюты (по умолчанию 30)
    var  limit: Int = 30
    let internetConetionChangetNotification: String = "internetConetionChangetNotification"
    
    /// ###Фильтры криптовалюты по умолчанию
    private var sort: String = "market_cap"
    private var criptocurensyType: String = "all"
    
    /// ###синглтон
    static let sharedDataController = DataController()
    
    let dataFormatter: DateFormatter = DateFormatter()
    
    
    private init() {
        reachability = Reachability()!
        requestManager = RequestManager.init()
        coreDataInterface = CoreDataInterface.init()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reachabilityChanged),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    /**
    Настройка лимита получения криптовалюты.
     - Параметры:
        - limit: число запрашиваймой криптовалюты
     */
    
    func set(limit: Int)  {
        self.limit = limit
    }
    
    func set(sort: String) {
        self.sort = sort
    }
    
    func set(cryptocurrencyType: String) {
        self.criptocurensyType = cryptocurrencyType
    }
    
     @objc  func reachabilityChanged() {
//          NotificationCenter.default.postNo
        NotificationCenter.default.post(Notification.init(name: Notification.Name.init(self.internetConetionChangetNotification)))
   
    }
    
     func checkInternet() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
//    func getGlobalInfo(completion: @escaping (_ result: GlobalInfo?) -> Void)  {
//        let parameters: Parameters = Parameters.init()
//        self.requestManager.request(request: "global-metrics/quotes/latest", parameters: parameters) { (globalInfo) in
//            completion(globalInfo as? GlobalInfo)
//        }
//    }
    
 
    func  getCoinsList(first: Int, completion: @escaping (_ result: Array<CoinViewModel>?) -> Void) {
        let parameters: Parameters = ["start": first, "limit": self.limit, "sort": sort, "cryptocurrency_type": criptocurensyType]
        self.requestManager.request(request: "cryptocurrency/listings/latest", parameters: parameters) { (response) in
            var coins: Array<CoinViewModel> = Array()
            if response != nil {
                for dataItem in response as! [Coin] {
                    let coin: CoinViewModel = CoinViewModel(coin: dataItem)
                    coins.append(coin)
                }
            }
            completion(coins)
        }
    }
}
