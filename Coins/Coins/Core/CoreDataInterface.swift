//
//  CoreDataInterface.swift
//  Coins
//
//  Created by Алексей Петров on 15/11/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataInterface {
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context: NSManagedObjectContext
    
    private enum EntityNames: String {
        case coinData = "CoinData"
        case quoteData = "QuoteData"
    }
    
    init() {
        context = appDelegate.persistentContainer.viewContext
    }
    
    private func isExist(id: Double!) -> Bool {
        let arrrayOfRecords = readRecords()!
        for record in arrrayOfRecords  {
            if record.id == id {
                return true
            }
        }
        return false
    }
    
    func write(coin: Coin!)  {
        createNewRecord(item: CoinCoreDataModel(coin: coin))
    }
    
   private func createNewRecord(item: CoinCoreDataModel!) {
    if !isExist(id: item.id) && item.name.count != 0 {
        let entityCoin = NSEntityDescription.entity(forEntityName: EntityNames.coinData.rawValue, in: context)
        var newCoinRecord = NSManagedObject(entity: entityCoin!, insertInto: context)
        newCoinRecord = item.createFromCoinCoreDataModel(newCoinRecord: newCoinRecord)
        do {
            try context.save()
            
        } catch {
            print("Failed saving")
        }
    }
    }
    
    func readRecords() -> Array<CoinCoreDataModel>! {
        var arrayWithCoins: Array = Array<CoinCoreDataModel>()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: EntityNames.coinData.rawValue)
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for coin in result as! [CoinData] {
                arrayWithCoins.append(CoinCoreDataModel(coin: coin))
            }
            
        } catch {
            
            print("Failed")
        }
        return arrayWithCoins
    }
    
}


