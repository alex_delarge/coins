//
//  CoinsDetailViewController.swift
//  Coins
//
//  Created by Алексей Петров on 30/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinsDetailViewController: UIViewController, CoinsDetailViewProtocol, UITabBarDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter: CoinsDetailPresenter!
    let configurator: CoinsDetailConfiguratorProtocol = CoinsDetailConfigurator()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        configurator.configure(with: self)
        presenter.configureView()
    }
    
     //UITableView - protocol methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.setNumbersOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: presenter.indeficators[indexPath.row], for: indexPath)
        return  presenter.configureCell(cell: cell, index: indexPath)
    }
    
    // CoinsDetailViewController protocol methods
    
    func setTitleName() {
        DispatchQueue.main.async {
            self.title  = self.presenter.setTitleText()
        }
    }
    
    func awakeFromStoryBoard (coin: Coin) {
        configurator.configure(with: self)
        presenter.configureView()
        presenter.startWith(coin: coin)
    }
    
    
}
