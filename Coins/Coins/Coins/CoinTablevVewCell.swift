//
//  CoinTablevVewCell.swift
//  Coins
//
//  Created by Алексей Петров on 13/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinTablevVewCell: UITableViewCell {
  
    var state: Bool = false
    
    //MARK: private
     private let largeStateNameSize: CGFloat = 20.0
    private let smallStateNameSize: CGFloat = 18.0
    
  
    
    
    //MARK: constraits
    @IBOutlet weak var symbolViewWidhtConstrait: NSLayoutConstraint!
    @IBOutlet weak var nameAndSymbolViewHeightConstrait: NSLayoutConstraint!
    @IBOutlet weak var buttonsViewHeightConstrait: NSLayoutConstraint!
    @IBOutlet weak var additonalInfoViewHeightConstrait: NSLayoutConstraint!
    @IBOutlet weak var dynamicCellsHightConstrait: NSLayoutConstraint!
    

    //MARK: outlets
    
//    @IBOutlet weak var weekDynamicGrowthView: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var weekDynamicLabel: UILabel!
    @IBOutlet weak var weekDynamicGrowthView: UIView!
    @IBOutlet weak var dayDynamicGrowthView: UIView!
    @IBOutlet weak var dayDynamicLabel: UILabel!
    @IBOutlet weak var weekDynamicView: UIView!
    @IBOutlet weak var dayDynamicView: UIView!
    @IBOutlet weak var hourDynamicLabel: UILabel!
    @IBOutlet weak var volume: UIStackView!
    @IBOutlet weak var total: UIStackView!
    @IBOutlet weak var circlating: UIStackView!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var growthLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var marketCapLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var circlatingSupplyLabel: UILabel!
    @IBOutlet weak var totalSupplyLabel: UILabel!
    @IBOutlet weak var volumeDayLabel: UILabel!
    @IBOutlet weak var additonalView: UIView!
    
    
    //MARK: enums
    private enum StatesConstraits: CGFloat {
        typealias RawValue = CGFloat
        case largeStateActivityElementsSize = 44.0
        case largeStateSymbolViewWidth = 49.0
        case smallStateNameAndSymbolViewHeight = 21.0
        case largeStateButtonsViewHeight = 42.0
        case largeStateAdditonalInfoViewHeight = 57
        case zeroState = 0.0
        case largeStateDynamicHieght = 137.0
        case smallStateDynamicHieght = 15.0
    }
    
//    enum State: Int {
//        case largeState
//        case smallState
//    }
//
    func setupGrowthLabel(number: Double) {
        if number > 0.0 {
            backView.backgroundColor = UIColor.growthColor
        } else {
            backView.backgroundColor = UIColor.downColor
        }
    }
    
    func setupAdditionalDynamicDay(number: Double) {
        dayDynamicLabel.text = String.doubleToString(double: number) + "%"
        if number > 0.0 {
//            backView.backgroundColor = UIColor.growthColor
            dayDynamicGrowthView.backgroundColor = UIColor.growthColor
            
        } else {
            dayDynamicGrowthView.backgroundColor = UIColor.downColor
        }
    }
    
    func setupAdditionalDynamicWeek(number: Double) {
        weekDynamicLabel.text = String.doubleToString(double: number) + "%"
        if number > 0.0 {
            weekDynamicGrowthView.backgroundColor = UIColor.growthColor
        } else {
            weekDynamicGrowthView.backgroundColor = UIColor.downColor
        }
    }
    
    func setLargeState() {
//        UIView.animate(withDuration: UIView.defaultDuration, animations: {
//            self.circlating.isHidden = false
//            self.total.isHidden = false
//            self.volume.isHidden = false
//        }) { (result) in
//
//        };
       
        UIView.animate(withDuration: UIView.defaultDuration) {
//            self.circlating.isHidden = false
            self.total.isHidden = false
            self.volume.isHidden = false
            self.additonalView.isHidden = false
            self.dayDynamicView.isHidden = false
            self.weekDynamicView.isHidden = false
            self.moreButton.isHidden = false
        }
         symbolViewWidhtConstrait.constant = StatesConstraits.largeStateSymbolViewWidth.rawValue
        dynamicCellsHightConstrait.constant = StatesConstraits.largeStateDynamicHieght.rawValue
        nameAndSymbolViewHeightConstrait.constant = StatesConstraits.largeStateActivityElementsSize.rawValue
        additonalInfoViewHeightConstrait.constant = StatesConstraits.largeStateAdditonalInfoViewHeight.rawValue
        buttonsViewHeightConstrait.constant = StatesConstraits.largeStateButtonsViewHeight.rawValue
        hourDynamicLabel.text = "Hour Dynam."
        UIView.animate(withDuration: UIView.defaultDuration, animations: {
            self.layoutIfNeeded()
        }) { (result) in
//            self.heightConstrait.constant = 4.0
            self.nameLabel.font = self.nameLabel.font.withSize(self.largeStateNameSize)
            self.nameLabel.animate(font: self.nameLabel.font, duration: UIView.defaultDuration) {
            }
        }
    }
    
    func setSmallState() {
        symbolViewWidhtConstrait.constant = StatesConstraits.zeroState.rawValue
        dynamicCellsHightConstrait.constant = StatesConstraits.smallStateDynamicHieght.rawValue
        nameAndSymbolViewHeightConstrait.constant = StatesConstraits.smallStateNameAndSymbolViewHeight.rawValue
        additonalInfoViewHeightConstrait.constant = StatesConstraits.zeroState.rawValue
        buttonsViewHeightConstrait.constant = StatesConstraits.zeroState.rawValue
        hourDynamicLabel.text = ""
        layoutIfNeeded()
        self.total.isHidden = true
        self.volume.isHidden = true
        self.additonalView.isHidden = true
        self.dayDynamicView.isHidden = true
        self.weekDynamicView.isHidden = true
        self.moreButton.isHidden = true
        nameLabel.font = nameLabel.font.withSize(smallStateNameSize)
        nameLabel.animate(font: nameLabel.font, duration: UIView.defaultDuration) {
        }
    }
    
    func changeState(isSelect: Bool, completion: @escaping () -> Void) {

    }
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super .setSelected(selected, animated: animated)
//        debugPrint("cell", selected)
//    }
    //MARK: Private methods
    

    private func resizeName(completion: @escaping () -> Void)  {
        nameLabel.font = nameLabel.font.withSize(largeStateNameSize)
        nameLabel.animate(font: nameLabel.font, duration: UIView.defaultDuration) {
            completion()
        }
    }
    
    override func awakeFromNib() {
//        state = State.smallState
        self.selectionStyle = .none
//        symbolView.isHidden = true
        setSmallState()
    }
    
    override func prepareForReuse() {
//        circlatingSupplyLabel.text = ""
        totalSupplyLabel.text = ""
        volumeDayLabel.text = ""
        growthLabel.text = ""
        priceLabel.text = ""
        symbolLabel.text = ""
        nameLabel.text = ""
        marketCapLabel.text = ""
        backView.backgroundColor = UIColor.clear
//        self.isSelected = false
        setSmallState()
//        self.symbolView.isHidden = true
//        nameLabel.transform = CGAffineTransform()
    }
}
