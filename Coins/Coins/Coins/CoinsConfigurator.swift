//
//  CoinsConfigurator.swift
//  Coins
//
//  Created by Алексей Петров on 28/10/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class CoinsConfigurator: CoinsConfiguratorProtocol {

    
    func configure(with viewController: CoinsViewController) {
        let presenter = CoinsPresenter(view: viewController)
        let interactor = CoinsInteractor(presenter: presenter)
        let router = CoinsRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor 
        presenter.router = router
    }
}
