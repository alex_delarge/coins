//
//  GlobalInfoConfigurator.swift
//  Coins
//
//  Created by Алексей Петров on 08/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation


class GlobalInfoConfigurator: GlobalInfoConfiguratorProtocol {
    
    func configure(with viewController: GlobalInfoViewController) {
        let presenter = GlobalInfoPresenter(view: viewController)
        let interactor = GlobalInfoIteractor(presenter: presenter)
        let router = GlobalInfoRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
